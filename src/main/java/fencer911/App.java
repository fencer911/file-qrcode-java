package fencer911;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
/**
 * @author <a href="https://github.com/fencer911/transfer-file-by-qrcode">transfer-file-by-qrcode</a>
 * @since 2020/03
 * A useful  QRCode tool !Generate QRCode for text or file.Then capture screen or camera to recognize  the packet and assemble it
 */
public class App extends JFrame {
    public static String APP_NAME="Gen QRCode for text or file ";
    public static App app;
    public static ExecutorService executor= Executors.newFixedThreadPool(3);

    public static void main(String[] args) {
        app = new App();
        app.setTitle(APP_NAME+" https://github.com/fencer911/transfer-file-by-qrcode");
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        if (screenSize.getWidth() <= 1366) {
            // 低分辨率下自动最大化窗口
            app.setExtendedState(JFrame.MAXIMIZED_BOTH);
        }
//        app.pack();
//        app.setVisible(true);

        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        app.setLocation(800, 0);
        app.setSize(800,500);
        MainWindow.getInstance().init();
        app.setContentPane(MainWindow.getInstance().getMainPanel());


        app.addListeners();
        app.setVisible(true);
    }
    public void initForm(){

    }
    public void addListeners() {
        FileQrCodeSenderListener.addListeners();
        FileQrCodeReceiverListener.addListeners();
    }
}
