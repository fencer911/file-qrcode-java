package fencer911;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import javax.swing.*;
import java.awt.*;

/**
 * @author <a href="https://github.com/fencer911/transfer-file-by-qrcode">transfer-file-by-qrcode</a>
 * @since 2020/03
 */

public class FileQrCodeReceiverForm {
    public static final int FILL_NONE = 0;
    public static final int FILL_HORIZONTAL = 1;
    public static final int FILL_VERTICAL = 2;
    public static final int FILL_BOTH = 3;
    public static final int ANCHOR_CENTER = 0;
    public static final int ANCHOR_NORTH = 1;
    public static final int ANCHOR_SOUTH = 2;
    public static final int ANCHOR_EAST = 4;
    public static final int ANCHOR_WEST = 8;
    public static final int ANCHOR_NORTHEAST = 5;
    public static final int ANCHOR_SOUTHEAST = 6;
    public static final int ANCHOR_SOUTHWEST = 10;
    public static final int ANCHOR_NORTHWEST = 9;
    public static final int SIZE_FIXED = 0;
    public static final int SIZE_CAN_SHRINK = 1;
    public static final int SIZE_CAN_GROW = 2;
    public static final int SIZE_WANT_GROW = 4;
    public static final int ALIGN_LEFT = 0;
    public static final int ALIGN_CENTER = 1;
    public static final int ALIGN_RIGHT = 2;
    public static final int ALIGN_FILL = 3;

    private JTextArea textArea;
    private JButton recButton;
    private JButton unReceivedButton;


    private JTextField captureRectTextField;



    private JPanel rootPanel;
    private JTextField intervalTextField;
    private ImagePannel leftPanel;

    private JRadioButton transModeFileRBtn;
    private JRadioButton transModeScreenRBtn;
    private JRadioButton transModeCameraRBtn;

    private JLabel tipLabel;

	public ImagePannel getLeftPanel() { return leftPanel; }

	public JTextArea getTextArea() {
		return textArea;
	}
	public JButton getRecButton() { return recButton; }
    public JButton getUnReceivedButton() { return unReceivedButton; }

	public JPanel getRootPanel() {
		return rootPanel;
	}
    public JTextField getCaptureRectTextField() {
        return captureRectTextField;
    }

    public JTextField getIntervalTextField() {
        return intervalTextField;
    }

    public JRadioButton getTransModeFileRBtn() { return transModeFileRBtn; }
    public JRadioButton getTransModeScreenRBtn() { return transModeScreenRBtn; }
    public JRadioButton getTransModeCameraRBtn() { return transModeCameraRBtn; }
    public JLabel getTipLabel() {
        return tipLabel;
    }

	private static FileQrCodeReceiverForm receiverForm;

	private FileQrCodeReceiverForm() {

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("二维码动态识别器");
        FileQrCodeReceiverForm qrCodeForm= FileQrCodeReceiverForm.getInstance();


        frame.setContentPane(qrCodeForm.getRootPanel());
        //frame.getContentPane().setSize(800, 800);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setSize(800,500);
        frame.setLocationRelativeTo(null);//在屏幕中居中显示
        //frame.pack();
        frame.setLocation(800, 0);
        frame.setVisible(true);
    }

    public static FileQrCodeReceiverForm getInstance() {
        if (receiverForm == null) {
            receiverForm = new FileQrCodeReceiverForm();
        }
        return receiverForm;
    }

    {
        $$$setupUI$$$();
    }

    private void $$$setupUI$$$() {
        JPanel bottomPanel= new JPanel();
        setBottomPanel(bottomPanel);
        bottomPanel.setBorder(BorderFactory.createTitledBorder("Qrcode Capture Config"));

        //mainPanel init start
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayoutManager(1, 7, new Insets(0, 0, 0, 0), -1, -1));

        //left
        leftPanel = new ImagePannel();
        leftPanel.setBorder(BorderFactory.createTitledBorder("Qrcode Img show"));

        //middle
        JPanel middlePannel = new JPanel();
        middlePannel.setBorder(BorderFactory.createTitledBorder("Operate Zone"));
        middlePannel.setLayout(new GridLayoutManager(4, 1));
        //sub modePannel
        transModeScreenRBtn = new JRadioButton("Capture Screen");
        transModeScreenRBtn.setSelected(true);
        transModeCameraRBtn = new JRadioButton("Capture Camera");

        transModeFileRBtn = new JRadioButton("File containing QR");
        ButtonGroup modeBG = new ButtonGroup();
        modeBG.add(transModeScreenRBtn);
        modeBG.add(transModeFileRBtn);
        modeBG.add(transModeCameraRBtn);

        JPanel modePanel = new JPanel(new GridLayoutManager(3, 1,new Insets(0, 0, 0, 0), -1, -1));
        modePanel.add(transModeScreenRBtn,new GridConstraints(0, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZE_CAN_SHRINK, SIZE_CAN_SHRINK , null, null, null, 0, false));
        modePanel.add(transModeFileRBtn,new GridConstraints(1, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZE_CAN_SHRINK, SIZE_CAN_SHRINK , null, null, null, 0, false));
        modePanel.add(transModeCameraRBtn,new GridConstraints(2, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZE_CAN_SHRINK, SIZE_CAN_SHRINK , null, null, null, 0, false));

        middlePannel.add(modePanel,new GridConstraints(0, 0, 1, 1, ANCHOR_NORTH, FILL_NONE, SIZE_CAN_SHRINK, SIZE_CAN_SHRINK , null, null, null, 0, false));
        middlePannel.add(recButton = new JButton("Recognition"),new GridConstraints(1, 0, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK , SIZE_CAN_SHRINK , null, null, null, 0, false));

        middlePannel.add(unReceivedButton = new JButton("Find lost"),new GridConstraints(2, 0, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK , SIZE_CAN_SHRINK , null, null, null, 0, false));
        middlePannel.add(tipLabel = new JLabel("Tips:"),new GridConstraints(3, 0, 1, 1, ANCHOR_NORTH, FILL_NONE, SIZE_CAN_SHRINK , SIZE_CAN_SHRINK , null, null, null, 0, false));

        //right
        final JScrollPane rightPanel = new JScrollPane();
        rightPanel.setViewportView(textArea = new JTextArea());
        rightPanel.setBorder(BorderFactory.createTitledBorder("Unreceived packet index number"));

        mainPanel.add(leftPanel,new GridConstraints(0, 0, 1, 4, ANCHOR_CENTER, FILL_BOTH, SIZE_CAN_SHRINK | SIZE_WANT_GROW, SIZE_CAN_SHRINK | SIZE_WANT_GROW, null, new Dimension(300,300), null, 0, false));
        mainPanel.add(middlePannel,new GridConstraints(0, 4, 1, 1, ANCHOR_CENTER, FILL_BOTH, SIZE_CAN_SHRINK , SIZE_WANT_GROW , null, null, null, 0, false));
        mainPanel.add(rightPanel,new GridConstraints(0, 5, 1, 2, ANCHOR_CENTER, FILL_BOTH, SIZE_CAN_SHRINK | SIZE_WANT_GROW, SIZE_CAN_SHRINK | SIZE_WANT_GROW, null, new Dimension(100,300), null, 0, false));

        //mainPanel init end

        rootPanel = new JPanel();
        rootPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(mainPanel, new GridConstraints(0, 0, 1, 1, ANCHOR_CENTER, FILL_BOTH, SIZE_CAN_SHRINK | SIZE_WANT_GROW, SIZE_CAN_SHRINK | SIZE_WANT_GROW, null, null, null, 0, false));

        rootPanel.add(bottomPanel, new GridConstraints(1, 0, 1, 1, ANCHOR_CENTER, FILL_BOTH, SIZE_CAN_SHRINK | SIZE_CAN_GROW, SIZE_CAN_SHRINK | SIZE_CAN_GROW, null, null, null, 0, false));
    }
    
    private void setBottomPanel(JPanel bottomPanel) {

        captureRectTextField = new JTextField("0,0,500,500");
        intervalTextField = new JTextField("40");

        bottomPanel.setLayout(new GridLayoutManager(1, 4, new Insets(5, 5, 5, 5), -1, -1));

        bottomPanel.add(new JLabel("Rectangle"), new GridConstraints(0, 0, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK, SIZE_FIXED, null, null, null, 0, false));
        bottomPanel.add(captureRectTextField, new GridConstraints(0, 1, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK|SIZE_WANT_GROW, SIZE_FIXED, null, null, null, 0, false));

        bottomPanel.add(new JLabel("Interval-Time"), new GridConstraints(0, 2, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK, SIZE_FIXED, null, null, null, 0, false));
        bottomPanel.add(intervalTextField, new GridConstraints(0, 3, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK, SIZE_FIXED, null, new Dimension(40, -1), null, 0, false));
    }
    public JComponent $$$getRootComponent$$$() {
        return rootPanel;
    }
    

}
