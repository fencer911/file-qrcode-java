package fencer911;

import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import javax.swing.*;
import java.awt.*;

/**
 * @author <a href="https://github.com/fencer911/transfer-file-by-qrcode">transfer-file-by-qrcode</a>
 * @since 2020/03
 */

public class FileQrCodeSenderForm {
    public static final int FILL_NONE = 0;
    public static final int FILL_HORIZONTAL = 1;
    public static final int FILL_VERTICAL = 2;
    public static final int FILL_BOTH = 3;
    public static final int ANCHOR_CENTER = 0;
    public static final int ANCHOR_NORTH = 1;
    public static final int ANCHOR_SOUTH = 2;
    public static final int ANCHOR_EAST = 4;
    public static final int ANCHOR_WEST = 8;
    public static final int ANCHOR_NORTHEAST = 5;
    public static final int ANCHOR_SOUTHEAST = 6;
    public static final int ANCHOR_SOUTHWEST = 10;
    public static final int ANCHOR_NORTHWEST = 9;
    public static final int SIZE_FIXED = 0;
    public static final int SIZE_CAN_SHRINK = 1;
    public static final int SIZE_CAN_GROW = 2;
    public static final int SIZE_WANT_GROW = 4;
    public static final int ALIGN_LEFT = 0;
    public static final int ALIGN_CENTER = 1;
    public static final int ALIGN_RIGHT = 2;
    public static final int ALIGN_FILL = 3;

    private JTextArea textArea;
    private JButton genButton;


    private JComboBox errorComboBox;
    private JTextField imgSizeTextField;
    private JTextField splitSizeTextField;
    private JTextField maxSizeTextField;

    private JPanel rootPanel;
    private JTextField intervalTextField;
    private ImagePannel leftPanel;
    private JRadioButton transModeFileRBtn;
    private JLabel tipLabel;

	public ImagePannel getLeftPanel() { return leftPanel; }

	public JTextArea getTextArea() {
		return textArea;
	}
	public JButton getGenButton() {
		return genButton;
	}

	public JPanel getRootPanel() {
		return rootPanel;
	}
    public JTextField getImgSizeTextField() {
        return imgSizeTextField;
    }
    public JTextField getSplitSizeTextField() {
        return splitSizeTextField;
    }
    public JTextField getIntervalTextField() {
        return intervalTextField;
    }
    public JTextField getMaxSizeTextField() { return maxSizeTextField; }

    public JComboBox getErrorComboBox() {
        return errorComboBox;
    }
    public JRadioButton getTransModeFileRBtn() {
        return transModeFileRBtn;
    }
    public JLabel getTipLabel() {
        return tipLabel;
    }

	private static FileQrCodeSenderForm senderForm;

	private FileQrCodeSenderForm() {
	
		
    }


    public static FileQrCodeSenderForm getInstance() {
        if (senderForm == null) {
            senderForm = new FileQrCodeSenderForm();
        }
        return senderForm;
    }

    {
        $$$setupUI$$$();
    }

    private void $$$setupUI$$$() {
        JPanel bottomPanel= new JPanel();
        setBottomPanel(bottomPanel);
        bottomPanel.setBorder(BorderFactory.createTitledBorder("Qrcode Gen Config"));

        //mainPanel init start
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayoutManager(1, 7, new Insets(0, 0, 0, 0), -1, -1));

        //left
        leftPanel = new ImagePannel();
        leftPanel.setBorder(BorderFactory.createTitledBorder("Qrcode Img show"));

        //middle
        JPanel middlePannel = new JPanel(new BorderLayout());
        middlePannel.setBorder(BorderFactory.createTitledBorder("Operate Zone"));
        middlePannel.setLayout(new GridLayoutManager(3, 1));
        //sub modePannel
        JRadioButton transModeText = new JRadioButton("Small  Text");
        transModeText.setSelected(true);
        transModeFileRBtn = new JRadioButton("File Mode ");
        ButtonGroup modeBG = new ButtonGroup();modeBG.add(transModeText);modeBG.add(transModeFileRBtn);
        JPanel modePannel = new JPanel(new GridLayoutManager(2, 1,new Insets(0, 0, 0, 0), -1, -1));
        modePannel.add(transModeText,new GridConstraints(0, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZE_CAN_SHRINK, SIZE_CAN_SHRINK , null, null, null, 0, false));
        modePannel.add(transModeFileRBtn,new GridConstraints(1, 0, 1, 1, ANCHOR_WEST, FILL_NONE, SIZE_CAN_SHRINK, SIZE_CAN_SHRINK , null, null, null, 0, false));

        middlePannel.add(modePannel,new GridConstraints(0, 0, 1, 1, ANCHOR_NORTH, FILL_NONE, SIZE_CAN_SHRINK, SIZE_CAN_SHRINK , null, null, null, 0, false));
        middlePannel.add(genButton = new JButton("Generate"),new GridConstraints(1, 0, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK , SIZE_CAN_SHRINK , null, null, null, 0, false));
        middlePannel.add(tipLabel = new JLabel("Tips:"),new GridConstraints(2, 0, 1, 1, ANCHOR_NORTH, FILL_NONE, SIZE_CAN_SHRINK , SIZE_CAN_SHRINK , null, null, null, 0, false));

        //right
        final JScrollPane rightPanel = new JScrollPane();
        rightPanel.setViewportView(textArea = new JTextArea());
        rightPanel.setBorder(BorderFactory.createTitledBorder("Text Or Range Input"));

        mainPanel.add(leftPanel,new GridConstraints(0, 0, 1, 4, ANCHOR_CENTER, FILL_BOTH, SIZE_CAN_SHRINK | SIZE_WANT_GROW, SIZE_CAN_SHRINK | SIZE_WANT_GROW, null, new Dimension(300,300), null, 0, false));
        mainPanel.add(middlePannel,new GridConstraints(0, 4, 1, 1, ANCHOR_CENTER, FILL_BOTH, SIZE_CAN_SHRINK , SIZE_WANT_GROW , null, null, null, 0, false));
        mainPanel.add(rightPanel,new GridConstraints(0, 5, 1, 2, ANCHOR_CENTER, FILL_BOTH, SIZE_CAN_SHRINK | SIZE_WANT_GROW, SIZE_CAN_SHRINK | SIZE_WANT_GROW, null, new Dimension(100,300), null, 0, false));

        //mainPanel init end

        rootPanel = new JPanel();
        rootPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(mainPanel, new GridConstraints(0, 0, 1, 1, ANCHOR_CENTER, FILL_BOTH, SIZE_CAN_SHRINK | SIZE_WANT_GROW, SIZE_CAN_SHRINK | SIZE_WANT_GROW, null, null, null, 0, false));

        rootPanel.add(bottomPanel, new GridConstraints(1, 0, 1, 1, ANCHOR_CENTER, FILL_BOTH, SIZE_CAN_SHRINK | SIZE_CAN_GROW, SIZE_CAN_SHRINK | SIZE_CAN_GROW, null, null, null, 0, false));
    }
    
    private void setBottomPanel(JPanel bottomPanel) {
        JLabel labelImgSize = new JLabel();labelImgSize.setText("Image-Size");
    	imgSizeTextField = new JTextField("300");


    	final JLabel labelError = new JLabel("Error-Correction-Level");
    	errorComboBox = new JComboBox();
            final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
            defaultComboBoxModel1.addElement(ErrorCorrectionLevel.L);
            defaultComboBoxModel1.addElement(ErrorCorrectionLevel.M);
            defaultComboBoxModel1.addElement(ErrorCorrectionLevel.Q);
            defaultComboBoxModel1.addElement(ErrorCorrectionLevel.H);
        errorComboBox.setModel(defaultComboBoxModel1);

        final JLabel labelSplit = new JLabel("Split-Size");
        splitSizeTextField = new JTextField("132");

        final JLabel labelFPS = new JLabel("Interval-Time");
        intervalTextField = new JTextField("40");

        bottomPanel.setLayout(new GridLayoutManager(1, 10, new Insets(5, 5, 5, 5), -1, -1));
    	bottomPanel.add(labelImgSize, new GridConstraints(0, 0, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK, SIZE_FIXED, null, null, null, 0, false));
    	bottomPanel.add(imgSizeTextField, new GridConstraints(0, 1, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK, SIZE_FIXED, null, new Dimension(40, -1), null, 0, false));


           bottomPanel.add(labelError, new GridConstraints(0, 2, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK, SIZE_FIXED, null, null, null, 0, false));
        bottomPanel.add(errorComboBox, new GridConstraints(0, 3, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK, SIZE_FIXED, null, null, null, 0, false));
    	   bottomPanel.add(labelSplit, new GridConstraints(0, 4, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK, SIZE_FIXED, null, null, null, 0, false));
   bottomPanel.add(splitSizeTextField, new GridConstraints(0, 5, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK, SIZE_FIXED, null, new Dimension(40, -1), null, 0, false));


              bottomPanel.add(labelFPS, new GridConstraints(0, 6, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK, SIZE_FIXED, null, null, null, 0, false));

        bottomPanel.add(intervalTextField, new GridConstraints(0, 7, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK, SIZE_FIXED, null, new Dimension(40, -1), null, 0, false));
        JLabel maxSizeLabel = new JLabel("Max-Text-Size");
        maxSizeTextField=new JTextField("200");
        bottomPanel.add(maxSizeLabel, new GridConstraints(0, 8, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK, SIZE_FIXED, null, null, null, 0, false));
        bottomPanel.add(maxSizeTextField, new GridConstraints(0, 9, 1, 1, ANCHOR_CENTER, FILL_NONE, SIZE_CAN_SHRINK, SIZE_FIXED, null, new Dimension(40, -1), null, 0, false));
	}
    public JComponent $$$getRootComponent$$$() {
        return rootPanel;
    }
    

}
