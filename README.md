

## 文件二维码 

利用二维码的动态生成与识别来传输数据（短文本，长文本，文件）， 理论大小不限，但越大越慢，小文件是可以接受的 有java与python实现版。

 本版本由于zxing的识别率不是很高(通过提供精确的截图区域坐标，可以解决部分无法识别的问题)，不打算继续开发！更高新的版本有Python版(FileQRCodePython)及对应的安卓实现，已放在CSDN:https://blog.csdn.net/fencer911/article/details/113391674?spm=1001.2014.3001.5501。



